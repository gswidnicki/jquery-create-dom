$(function(){
  //funkcja generuje nagłówek
  function createElementH(hNumber, hText) {
    return $('<h'+hNumber+'>', { html: hText} );
  }
  //funkcja generuje listę opcji wyboru nagłówka
  function createElementSelect(hNumber) {
    return $('<option>', { html: hNumber } );
  }
  //funkcja generuje treść artykułu
  function createElementTextArticle(tArticle) {
    return $('<p>', { html: tArticle } );
  }
  //funkcja generuje w DOM opcje wyboru nagłówka od 2 do 6,
  //ponieważ nagłówek h1 został już użyty jako nagłówek formularza
  $('#hNumber').append([2,3,4,5,6].map(h => createElementSelect(h)));
  function createArticle(hNumber, hText, tArticle) {
    return $('#headers').append(createElementH(hNumber, hText)).append(createElementTextArticle(tArticle));
  }
  'use strict';
  var formCreateArticle = document.getElementById('formCreateArticle');
  formCreateArticle.addEventListener('submit', function (e) {
    e.preventDefault();
    var hText = document.getElementById('hText').value;
    var hNumber = document.getElementById('hNumber').value;
    var tArticle = document.getElementById('tArticle').value;
    if (!hText) {
      alert("Podaj tekst nagłówka!");
    }else {
      createArticle(hNumber, hText, tArticle);
      //$('#headers').append(createArticle(hNumber, hText, tArticle));
      //$('#headers').append(createElementH(hNumber, hText));
    }
  });
});
